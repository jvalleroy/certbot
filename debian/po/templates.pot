# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the python-certbot package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: python-certbot\n"
"Report-Msgid-Bugs-To: python-certbot@packages.debian.org\n"
"POT-Creation-Date: 2020-06-21 21:24-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../certbot.templates:1001
msgid "You have unexpired certs; do you still want to purge?"
msgstr ""

#. Type: boolean
#. Description
#: ../certbot.templates:1001
msgid ""
"The certbot configuration directory /etc/letsencrypt still contains "
"unexpired certificates that may be live on your system.  If you choose this "
"option, the purge will continue and delete those certificates, potentially "
"breaking services which depend on them."
msgstr ""

#. Type: boolean
#. Description
#: ../certbot.templates:1001
msgid ""
"If you have already installed different certificates in your services, or "
"you have confirmed you don't have any services depending on these "
"certificates, you should choose this option.  To cancel and manually delete "
"the /etc/letsencrypt directory, you should refuse this option."
msgstr ""
